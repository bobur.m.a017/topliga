package com.topliga.bot;

import com.topliga.bot.botService.buttons.ButtonService;
import com.topliga.bot.botService.userService.UserService;
import com.topliga.service.BasedService;
import com.topliga.service.StandingsService;
import com.topliga.service.TournamentService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;

@Component
public class Main extends TelegramLongPollingBot {

    private final ButtonService buttonService;
    private final UserService userService;
    private final StandingsService standingsService;
    private final TournamentService tournamentService;
    private final BasedService basedService;

    @Autowired
    public Main(ButtonService buttonService, UserService userService, StandingsService standingsService, TournamentService tournamentService, BasedService basedService) {

        this.buttonService = buttonService;
        this.userService = userService;
        this.standingsService = standingsService;
        this.tournamentService = tournamentService;
        this.basedService = basedService;
    }

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            switch (update.getMessage().getText()){

                case "/start":
                    userService.saveUser(update);
                case "Orqaga ↩️":
                    ReplyKeyboardMarkup replyKeyboardMarkup = buttonService.mainMenu();
                    execute(null, replyKeyboardMarkup, update.getMessage().getChatId(), "bo'limni tanlang");
                    break;
                case "Chempionatlar":
                    execute(null, buttonService.getLeagues(), update.getMessage().getChatId(), "Chempionatni tanlang");
                    break;
                case "World":
                case "England":
                case "France":
                case "Brazil":
                case "Italy":
                case "Germany":
                case "Spain":
                case "Uzbekistan":
                    execute(null, buttonService.getWorlds(update.getMessage().getText()), update.getMessage().getChatId(), "Turnirni tanlang");
                    break;
                case "Obunchilar Soni":
                    try {
                        execute(userService.countUsers(update.getMessage().getChatId()));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                    break;
                case "Biz Haqimizda":
                    execute(userService.aboutUs(update));
                    break;
                case "Ligue 1":
                case "Serie A":
                case "Bundesliga":
                case "La Liga":
                case "Super League":
                case "UEFA Europa League":
                case "Premier League":
                case "UEFA Champions League":
                    execute(standingsService.getStandings(update.getMessage().getChatId(),update.getMessage().getText()));
                    break;
                case "O'tgan O'yinlar":
                    execute(tournamentService.last10Match(update));
                    break;
                case "Asosiy o'yinlar":
                    execute(tournamentService.liveScore(update));
                    break;

                default:
                    ReplyKeyboardMarkup replyKeyboardMarkup1 = buttonService.mainMenu();
                    execute(null, replyKeyboardMarkup1, update.getMessage().getChatId(), "Notogri buyruq kiritildi");
            }

        }else if(update.hasCallbackQuery()){
            execute(tournamentService.tournamentType(update));
        }

    }

    @Override
    public String getBotUsername() {
        return "futboluz_uz_bot";
    }

    @Override
    public String getBotToken() {
        return "2014587724:AAEiN00E3k2ModMS9DWP0xLfrm46vvtRtk0";
    }

    public void execute(InlineKeyboardMarkup inlineKeyboardMarkup, ReplyKeyboardMarkup replyKeyboardMarkup, Long chatId, String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(chatId));
        sendMessage.setText(text);
        if (inlineKeyboardMarkup == null) {
            replyKeyboardMarkup.setResizeKeyboard(true);
            replyKeyboardMarkup.setOneTimeKeyboard(true);
            replyKeyboardMarkup.setSelective(true);
            replyKeyboardMarkup.setOneTimeKeyboard(false);
            sendMessage.setReplyMarkup(replyKeyboardMarkup);
        } else {
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        }
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
