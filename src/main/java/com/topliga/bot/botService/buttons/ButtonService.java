package com.topliga.bot.botService.buttons;

import com.topliga.entity.country.Country;
import com.topliga.entity.tournament.Tournament;
import com.topliga.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ButtonService {
    private final CountryRepository countryRepository;

    @Autowired
    public ButtonService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;

    }

    public ReplyKeyboardMarkup mainMenu() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboardRowList = new ArrayList<>();

        KeyboardRow keyboardRow = new KeyboardRow();
        KeyboardButton keyboardButton = new KeyboardButton();
        keyboardButton.setText("Chempionatlar");
        keyboardRow.add(keyboardButton);
        keyboardRowList.add(keyboardRow);

        keyboardRow = new KeyboardRow();
        keyboardButton = new KeyboardButton();

        keyboardButton.setText("O'tgan O'yinlar");
        keyboardRow.add(keyboardButton);
        keyboardButton = new KeyboardButton();
        keyboardButton.setText("Asosiy o'yinlar");
        keyboardRow.add(keyboardButton);
        keyboardRowList.add(keyboardRow);

        keyboardRow = new KeyboardRow();
        keyboardButton = new KeyboardButton();

        keyboardButton.setText("Obunchilar Soni");
        keyboardRow.add(keyboardButton);
        keyboardButton = new KeyboardButton();
        keyboardButton.setText("Biz Haqimizda");
        keyboardRow.add(keyboardButton);
        keyboardRowList.add(keyboardRow);

        replyKeyboardMarkup.setKeyboard(keyboardRowList);

        return replyKeyboardMarkup;
    }

    public ReplyKeyboardMarkup getLeagues() {
        List<Country> countryList = countryRepository.findAll();
        return buttonMaker(countryList);
    }

    public ReplyKeyboardMarkup getWorlds(String countryName) {
        Optional<Country> countryByName = countryRepository.getCountryByName(countryName);
        if (countryByName.isPresent()) {
            List<Tournament> tournaments = countryByName.get().getTournaments();
            return buttonMaker(tournaments);
        }
        return null;

    }

    public ReplyKeyboardMarkup buttonMaker(List list) {

        String name = "";
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        int count = 0;
        List<KeyboardRow> keyboardRowList = new ArrayList<>();
        KeyboardRow keyboardRow = new KeyboardRow();
        KeyboardButton keyboardButton = new KeyboardButton();

        for (Object obj : list) {
            if (obj instanceof Tournament) {
                name = ((Tournament) obj).getName();

            } else if (obj instanceof Country) {
                name = ((Country) obj).getName();

            }
            count++;
            if (count % 3 == 0) {
                keyboardRowList.add(keyboardRow);
                keyboardRow = new KeyboardRow();
                count++;
            }
            keyboardButton.setText(name);
            keyboardRow.add(keyboardButton);
            keyboardButton = new KeyboardButton();
        }
        if (!keyboardRow.isEmpty()) {
            keyboardRowList.add(keyboardRow);
        }
        keyboardButton.setText("Orqaga ↩️");
        keyboardRow = new KeyboardRow();
        keyboardRow.add(keyboardButton);
        keyboardRowList.add(keyboardRow);
        replyKeyboardMarkup.setKeyboard(keyboardRowList);
        return replyKeyboardMarkup;
    }
}
