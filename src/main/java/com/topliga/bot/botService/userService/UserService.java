package com.topliga.bot.botService.userService;

import com.topliga.entity.user.Users;
import com.topliga.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UsersRepository usersRepository;

    public void saveUser(Update update) {
        Long chatId = update.getMessage().getChatId();
        Optional<Users> users = usersRepository.findByChatId(chatId);
        Users user = new Users();
        if (users.isEmpty()) {
            user.setChatId(chatId);
            user.setUsername(update.getMessage().getFrom().getUserName());
            user.setName(update.getMessage().getFrom().getFirstName());
            usersRepository.save(user);
        }

    }
    public SendMessage countUsers(Long chatId){
        List<Users> usersList = usersRepository.findAll();
        SendMessage sendMessage=new SendMessage();
        sendMessage.setChatId(chatId.toString());
        sendMessage.setText("Hozirdagi faol obunachilar soni "+usersList.size()+" ta");
        return sendMessage;
    }
    public SendMessage aboutUs(Update update){
        SendMessage sendMessage=new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId().toString());
        String text="Assalomu alaykum !\n"+
                "\n Ushbu @futboluz_uz_bot kichik loyihasi futbol ishqibozlari uchun " +
                "\n @Abdivaliev hamda @bobur0017 tomonidan yaratildi."+
                "\n Xato va kamchiliklar yoki takliflar uchun yuqoridagi " +
                "\n usernamelardan biriga murojat qilishingiz mumkin!"+
                "\n Biz bilan qolaganigiz uchun tashakkur!";
        sendMessage.setText(text);
        return sendMessage;
    }
}
