package com.topliga;

import com.topliga.DTO.enums.NameOfTournaments;
import com.topliga.repository.AdminRepository;
import com.topliga.repository.CountryRepository;
import com.topliga.entity.admin.Admin;
import com.topliga.entity.admin.Role;
import com.topliga.repository.MatchRepository;
import com.topliga.repository.RoleRepository;
import com.topliga.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;
import java.util.Optional;
@EnableScheduling
@SpringBootApplication
public class TopligaApplication implements CommandLineRunner {
    final StatsServices statsServices;
    final MatchService matchService;
    final PlayerService playerService;
    final StandingsService standingsService;
    final MatchRepository matchRepository;
    final BasedService basedService;
    final GamesServices gamesServices;
    final CountryRepository countryRepository;
    final RoleRepository roleRepository;
    final AdminRepository adminRepository;
    final PasswordEncoder passwordEncoder;

    @Autowired
    public TopligaApplication(StatsServices statsServices, MatchService matchService,
                              PlayerService playerService, StandingsService standingsService,
                              MatchRepository matchRepository, BasedService basedService,
                              GamesServices gamesServices, CountryRepository countryRepository, RoleRepository roleRepository, AdminRepository adminRepository, PasswordEncoder passwordEncoder)
    {
        this.statsServices = statsServices;
        this.matchService = matchService;
        this.playerService = playerService;
        this.standingsService = standingsService;
        this.matchRepository = matchRepository;
        this.basedService = basedService;
        this.gamesServices = gamesServices;
        this.countryRepository = countryRepository;
        this.roleRepository = roleRepository;
        this.adminRepository = adminRepository;
        this.passwordEncoder = passwordEncoder;
    }


    public static void main(String[] args) {
        SpringApplication.run(TopligaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        statsServices.getStats("Premier League");
//        basedService.getOldMatch("Primera Division");
//         gamesServices.parserLiveScore();
//        inserAdmin();
//        baseInsert();
//        relation();
    }
    @Scheduled(cron = "0 0 6,22 * * *",zone = "Asia/Tashkent")
    public void baseInsert2() {
        for (NameOfTournaments value : NameOfTournaments.values()) {
            standingsService.getStandings(value.getLeagueId());
            matchService.getMatches(value.getLeagueId());
        }
        for (NameOfTournaments value : NameOfTournaments.values()) {
            playerService.getTopAssists(value.getLeagueId());
            playerService.getTopScorers(value.getLeagueId());
        }

    }

    public void baseInsert() {
        for (NameOfTournaments value : NameOfTournaments.values()) {
            standingsService.getStandings(value.getLeagueId());
            matchService.getMatches(value.getLeagueId());
        }
        for (NameOfTournaments value : NameOfTournaments.values()) {
            playerService.getTopAssists(value.getLeagueId());
            playerService.getTopScorers(value.getLeagueId());
        }

    }

    public void relation() {
        standingsService.matchesToTeam();
        matchService.matchToTeam();
        standingsService.teamToTournament();
        standingsService.tournamentToCountry();
    }

    public void inserAdmin(){
        Admin admin=new Admin();
        Role role=new Role();
        Optional<Role> super_admin = roleRepository.findByName("SUPER_ADMIN");
        if (super_admin.isPresent()) {
            role=super_admin.get();
        }else {
            role.setId(1);
            role.setName("SUPER_ADMIN");
            role= roleRepository.save(role);
        }

        admin.setAdminName("bobur0017");
        admin.setPhoneNumber("+998993606667");
        admin.setPassword(passwordEncoder.encode("1234567abc"));
        Optional<Admin> byPhoneNumber = adminRepository.findByPhoneNumber("+998993606667");
        if (byPhoneNumber.isEmpty()) {
            Admin save = adminRepository.save(admin);
            save.setRoles(Collections.singletonList(role));
            adminRepository.save(save);
        }
    }
}
