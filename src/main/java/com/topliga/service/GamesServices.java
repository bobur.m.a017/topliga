package com.topliga.service;

import com.topliga.entity.match.LiveMatches;
import com.topliga.repository.LiveMatchesRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Component
public class GamesServices {
    @Autowired
    private final LiveMatchesRepository liveMatchesRepository;

    public GamesServices(LiveMatchesRepository liveMatchesRepository) {
        this.liveMatchesRepository = liveMatchesRepository;
    }

    @Scheduled(fixedDelay = 1000*60*2)
    public void parserLiveScore() throws IOException {

        Document document = Jsoup.connect("https://soccer365.ru/").get();
        Elements teams1 = document.getElementsByAttributeValue("class", "game_block");
        Elements teamsOnline = document.getElementsByAttributeValue("class", "game_block online");

        int count=0;
        for (org.jsoup.nodes.Element element : teamsOnline) {

            Elements goals = element.getElementsByAttributeValue("class", "gls");
            Elements status = element.getElementsByAttributeValue("class", "status");

            Elements tournamentName = element.getElementsByAttributeValue("class", "cmp");
            Elements tournamentNamme1 = tournamentName.get(0).getElementsByAttributeValue("class", "img16");


            Elements team1 = element.getElementsByAttributeValue("class", "ht");
            Elements teamsName1 = team1.get(0).getElementsByAttributeValue("class", "img16");
            Elements team2 = element.getElementsByAttributeValue("class", "at");
            Elements teamsName2 = team2.get(0).getElementsByAttributeValue("class", "img16");
            Optional<LiveMatches> liveMatch = liveMatchesRepository.findById(count);
            LiveMatches liveMatches;
            liveMatches = liveMatch.orElseGet(LiveMatches::new);
            liveMatches.setGoals(goals.text());
            liveMatches.setId(count);
            liveMatches.setTime(status.text());
            liveMatches.setTournamentName(tournamentNamme1.text());
            teamsName1.forEach(element1 -> {
                String src = element1.child(1).absUrl("src");
                liveMatches.setLogoTeamH(src);
            });
            teamsName2.forEach(element2 -> {
                String src2 = element2.child(0).absUrl("src");
                liveMatches.setLogoTeamA(src2);
            });
            liveMatches.setTeamH(teamsName1.text());
            liveMatches.setTeamA(teamsName2.text());
            liveMatchesRepository.save(liveMatches);
            count++;
        }

        for (int i = count; i < teams1.toArray().length; i++) {
            Elements goals = teams1.get(i).getElementsByAttributeValue("class", "gls");
            Elements status = teams1.get(i).getElementsByAttributeValue("class", "status");
            Elements tournamentName = teams1.get(i).getElementsByAttributeValue("class", "cmp");
            Elements tournamentNamme1 = tournamentName.get(0).getElementsByAttributeValue("class", "img16");

            Elements team1 = teams1.get(i).getElementsByAttributeValue("class", "ht");
            Elements teamsName1 = team1.get(0).getElementsByAttributeValue("class", "img16");

            Elements team2 = teams1.get(i).getElementsByAttributeValue("class", "at");
            Elements teamsName2 = team2.get(0).getElementsByAttributeValue("class", "img16");


            Optional<LiveMatches> liveMatch = liveMatchesRepository.findById(count);
            LiveMatches liveMatches;
            liveMatches = liveMatch.orElseGet(LiveMatches::new);
            liveMatches.setGoals(goals.text());
            liveMatches.setId(count);
            liveMatches.setTime(getStatusTime(status.text()));
            liveMatches.setTournamentName(tournamentNamme1.text());
            liveMatches.setTeamH(teamsName1.text());
            liveMatches.setTeamA(teamsName2.text());
            teamsName1.forEach(element1 -> {
                String src = element1.child(1).absUrl("src");
                liveMatches.setLogoTeamH(src);
            });
            teamsName2.forEach(element2 -> {
                String src2 = element2.child(0).absUrl("src");
                liveMatches.setLogoTeamA(src2);
            });
            liveMatchesRepository.save(liveMatches);
            count++;
        }
    }

    public String getMatchesLive(String index2) {
        int index = Integer.parseInt(index2);
        StringBuilder text = new StringBuilder();
        List<LiveMatches> liveMatchesList = liveMatchesRepository.findAllByOrderByIdAsc();
        int count = 0;
        for (int i = index; i < liveMatchesList.size(); i++) {
            text.append(liveMatchesList.get(i).getTournamentName()).append("\n");
            text.append(liveMatchesList.get(i).getTime()).append("\n");
            text.append(liveMatchesList.get(i).getTeamH()).append(" ").append(liveMatchesList.get(i).getGoals()).append(" ");
            text.append(liveMatchesList.get(i).getTeamA()).append("\n\n");
            if (count == 9) {
                break;
            }
            count++;
        }
        return text.toString();
    }
    public String getStatusTime(String status){
        String statusHas=status;
        if (!status.equals("Завершен")){
            if (status.length()>6){
                statusHas=status.substring(0,status.length()-6);
               LocalTime localTime=LocalTime.parse(status.substring(status.length()-5));
               LocalTime localTime1 = localTime.plusHours(2);
               statusHas+=localTime1.toString();
            }else {
                LocalTime localTime=LocalTime.parse(status.substring(status.length()-5));
                LocalTime localTime1 = localTime.plusHours(2);
                statusHas=localTime1.toString();
            }
        }

        return statusHas;
    }
}
