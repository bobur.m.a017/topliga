package com.topliga.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.topliga.DTO.standingsDTO.StandingsDTO;
import com.topliga.DTO.standingsDTO.main.Response;
import com.topliga.DTO.standingsDTO.main.league.league.standings.Standings;
import com.topliga.bot.botService.buttons.ButtonService;
import com.topliga.bot.botService.buttons.InlineKeyboardService;
import com.topliga.entity.country.Country;
import com.topliga.entity.match.Match;
import com.topliga.entity.team.Team;
import com.topliga.entity.team.teamInfo.TeamInfo;
import com.topliga.entity.tournament.Tournament;
import com.topliga.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class StandingsService implements Token{
    private final TeamRepository teamRepository;
    private final TeamInfoRepository teamInfoRepository;
    private final MatchRepository matchRepository;
    private final TournamentRepository tournamentRepository;
    private final CountryRepository countryRepository;
    private final InlineKeyboardService inlineKeyboardService;
    private final ButtonService buttonService;

    @Autowired
    public StandingsService(TeamRepository teamRepository, TeamInfoRepository teamInfoRepository, MatchRepository matchRepository, TournamentRepository tournamentRepository, CountryRepository countryRepository, InlineKeyboardService inlineKeyboardService, ButtonService buttonService) {
        this.teamRepository = teamRepository;
        this.teamInfoRepository = teamInfoRepository;
        this.matchRepository = matchRepository;
        this.tournamentRepository = tournamentRepository;
        this.countryRepository = countryRepository;
        this.inlineKeyboardService = inlineKeyboardService;
        this.buttonService = buttonService;
    }

    public void getStandings(int leagueId) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://api-football-v1.p.rapidapi.com/v3/standings?season=2022&league=" + leagueId))
                .header("x-rapidapi-host", "api-football-v1.p.rapidapi.com")
                .header("x-rapidapi-key", getToken())
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = null;
        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        ObjectMapper objectMapper = new ObjectMapper();
        StandingsDTO standingsDTO = new StandingsDTO();
        try {
            assert response != null;
            standingsDTO = objectMapper.readValue(response.body(), StandingsDTO.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        standingsInsert(standingsDTO);
    }

    public void standingsInsert(StandingsDTO standingsDTO) {
        List<Response> response = standingsDTO.getResponse();

        for (Response response1 : response) {
            String tournament_name = response1.getLeague().getName();
            String country_name = response1.getLeague().getCountry();
            Integer season = response1.getLeague().getSeason();
            Optional<Tournament> optionalTournament = tournamentRepository.findById(response1.getLeague().getId());
            Tournament tournament;
            if (optionalTournament.isEmpty()) {
                tournament = new Tournament();
            } else
                tournament = optionalTournament.get();
            Optional<Country> countryByName = countryRepository.getCountryByName(response1.getLeague().getCountry());
            Country country;
            if (countryByName.isEmpty()) {
                country = new Country();
            } else {
                country = countryByName.get();
            }
            country.setLogo(response1.getLeague().getFlag());
            country.setName(response1.getLeague().getCountry());
            countryRepository.save(country);

            tournament.setId(response1.getLeague().getId());
            tournament.setSeason(response1.getLeague().getSeason());
            tournament.setName(response1.getLeague().getName());
            tournament.setCountryName(response1.getLeague().getCountry());
            tournament.setLogo(response1.getLeague().getLogo());
            tournamentRepository.save(tournament);

            List<List<Standings>> standings1 = response1.getLeague().getStandings();
            for (List<Standings> standing : standings1) {
                for (Standings standings : standing) {
                    Team team;
                    Optional<Team> optionalTeam = teamRepository.findByTeamIdAndTournamentName(standings.getTeam().getId(),tournament_name);
                    team = optionalTeam.orElseGet(Team::new);
                    Integer teamId = standings.getTeam().getId();
                    TeamInfo teamInfo = new TeamInfo();
                    team.setTeamId(teamId);
                    team.setDate(standings.getUpdate());
                    team.setGoalsDiff(standings.getGoalsDiff());
                    team.setForm(standings.getForm());
                    team.setPoint(standings.getPoints());
                    team.setGroup(standings.getGroup());
                    team.setForm(standings.getForm());
                    team.setStanding(standings.getRank());
                    team.setName(standings.getTeam().getName());
                    team.setLogo(standings.getTeam().getLogo());
                    team.setCountry(country_name);
                    team.setTournamentName(tournament_name);
                    team.setSeason(season);
                    TeamInfo teamInfos = team.getTeamInfos();
                    if (teamInfos == null) {
                        teamInfos = teamInfo;
                    }
                    teamInfos.setDraw(standings.getAll().getDraw());
                    teamInfos.setGoalsAgainst(standings.getAll().getGoals().getGoalsAgainst());
                    teamInfos.setGoalsFor(standings.getAll().getGoals().getGoalsFor());
                    teamInfos.setWin(standings.getAll().getWin());
                    teamInfos.setLose(standings.getAll().getLose());
                    teamInfos.setPlayed(standings.getAll().getPlayed());
                    teamInfoRepository.save(teamInfos);
                    team.setTeamInfos(teamInfos);
                    teamRepository.save(team);
                }
            }
        }
    }

    public void matchesToTeam() {
        List<Team> teamList = teamRepository.findAll();
        List<Match> matchList = matchRepository.findAll();
        List<Match> matches1 = new ArrayList<>();
        teamList.forEach(team -> {
            ArrayList<Match> matches = new ArrayList<>();
            matchList.forEach(match -> {
                if (match.getHome().getTeamId().equals(team.getId()) || match.getAway().getTeamId().equals(team.getId())) {
                    matches.add(match);
                    matches1.add(match);
                }
            });
            team.setMatch(matches);
            teamRepository.save(team);

        });
        System.out.println(matches1.size());
    }

    public void teamToTournament() {
        List<Team> teamList = teamRepository.findAll();
        List<Tournament> tournamentList = tournamentRepository.findAll();
        for (Tournament tournament : tournamentList) {
            List<Team> teams = teamList.stream().map(team -> {
                if (team.getTournamentName().equals(tournament.getName())) {
                    return team;
                }
                return null;
            }).collect(Collectors.toList());
            tournament.setTeams(teams);
            tournamentRepository.save(tournament);
        }
    }

    public void tournamentToCountry() {
        List<Tournament> tournamentList = tournamentRepository.findAll();
        List<Country> countryList = countryRepository.findAll();
        for (Tournament tournament : tournamentList) {
            for (Country country : countryList) {
                if (tournament.getCountryName().equals(country.getName())) {
                    tournament.setCountry(country);
                    tournamentRepository.save(tournament);
                }
            }
        }
    }


    public String[] getTournamentName(String str) {                ///////////
        String[] s = str.split("_");
        return s;
    }

    public SendMessage getStandings(Long chatId, String tournamentName) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setParseMode(ParseMode.HTML);
        sendMessage.setChatId(chatId.toString());
        StringBuilder text = getTableTournament(tournamentName);
//        text.append("P-o'yinlar soni\n W-g'alabalar\n D-durranj");
        System.out.println(text);
        sendMessage.setText("<pre>" + text + "</pre>");
        Integer defaultIndex = null;
        List<Tournament> tournamentList = tournamentRepository.findAll();
        for (int i = 0; i < tournamentList.size(); i++) {
            if (tournamentList.get(i).getName().equals(tournamentName)) {
                defaultIndex=i;
            }
        }
        InlineKeyboardMarkup inlineKeyboardMarkup = inlineKeyboardService.keyboardForStatisticTable(tournamentName,defaultIndex);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        return sendMessage;
    }


    public StringBuilder getTableTournament(String tournamentName){
        StringBuilder text = new StringBuilder();//30->12=ClubName 2-># 3->Space 5*3
        int count = 1;


        List<Team> teamList = teamRepository.findAllByTournamentNameOrderByGroupAscPointDesc(tournamentName);

        String groupNmae = "";
        for (Team team : teamList) {
            if (!groupNmae.equals(team.getGroup())) {
                count = 1;
                groupNmae = team.getGroup();
                text.append("\n\n").append(team.getGroup()).append("\n");
                text.append("№    Club       O' O G'  M Gol").append("\n").append("------------------------------");
            }
            text.append("\n");
            if (count < 10) {
                text.append(count).append(" ");
            } else {
                text.append(count);
            }

            if (team.getName().length() > 12) {
                text.append(team.getName(), 0, 12);

            } else {
                int length = 12 - team.getName().length();
                text.append(team.getName());
                text.append(" ".repeat(Math.max(0, length)));
            }


            if (team.getTeamInfos().getPlayed() < 10) {
                text.append("  ").append(team.getTeamInfos().getPlayed());
            } else {
                text.append(" ").append(team.getTeamInfos().getPlayed());
            }


            if (team.getPoint() < 10) {
                text.append("  ").append(team.getPoint());
            } else {
                text.append(" ").append(team.getPoint());
            }


            if (team.getTeamInfos().getWin() < 10) {
                text.append("  ").append(team.getTeamInfos().getWin());
            } else {
                text.append(" ").append(team.getTeamInfos().getWin());
            }


//            if (team.getTeamInfos().getDraw()<10){
//                text.append("  ").append(team.getTeamInfos().getDraw());
//            }else {
//                text.append(" ").append(team.getTeamInfos().getDraw());
//            }


            if (team.getTeamInfos().getLose() < 10) {
                text.append("  ").append(team.getTeamInfos().getLose());
            } else {
                text.append(" ").append(team.getTeamInfos().getLose());
            }


            if (team.getGoalsDiff() < 10 && team.getGoalsDiff()>=0) {
                text.append("  ").append(team.getGoalsDiff());
            } else {
                text.append(" ").append(team.getGoalsDiff());
            }


            count++;

        }
        return text;
    }

    public String getLast100Matches(String index){

        int indexInt = Integer.parseInt(index);
        List<Match> last100Match = matchRepository.findLast100Match();
        StringBuilder text= new StringBuilder();
        int count=0;
        for (int i = indexInt; i < last100Match.size(); i++) {
            Match match = last100Match.get(i);
            text.append(match.getTournaments().getName()).append("\n");
            text.append(match.getRound()).append("\n");
            text.append(getDate(match.getTime())).append("\n");
            text.append(match.getHome().getTeamName()).append("  ").append(match.getHome().getGoals())
                    .append(":").append(match.getAway().getGoals()).append("  ").append(match.getAway().getTeamName()).append("\n\n");
        if (count==10){
            return text.toString();
        }
        count++;

        }


        return text.toString();
    }
    public  String  getDate(Timestamp date1){
        long time = date1.getTime();
        Date date=new Date(time * 1000L);
        SimpleDateFormat newDate=new SimpleDateFormat("yyyy-MM-dd HH:mm");
        newDate.setTimeZone(TimeZone.getTimeZone("Asia/Tashkent"));
        return newDate.format(date);
    }
}

