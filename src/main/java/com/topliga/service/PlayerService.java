package com.topliga.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.topliga.DTO.playersDTO.PlayersDTO;
import com.topliga.entity.player.Player;
import com.topliga.entity.team.Team;
import com.topliga.entity.tournament.Tournament;
import com.topliga.repository.PlayerRepository;
import com.topliga.repository.TeamRepository;
import com.topliga.repository.TournamentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Optional;

@Service
public class PlayerService implements  Token{
    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    TeamRepository teamRepository;

    @Autowired
    TournamentRepository tournamentRepository;

    @Autowired
    private final BasedService basedService;

    public PlayerService(BasedService basedService) {
        this.basedService = basedService;
    }

    public void getTopScorers(int leagueId) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://api-football-v1.p.rapidapi.com/v3/players/topscorers?league="+leagueId+"&season=2022"))
                .header("x-rapidapi-host", "api-football-v1.p.rapidapi.com")
                .header("x-rapidapi-key", getToken())
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = null;
        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        assert response != null;
        ObjectMapper objectMapper = new ObjectMapper();
        PlayersDTO playersDTO = new PlayersDTO();
        try {
            playersDTO = objectMapper.readValue(response.body(), PlayersDTO.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        insertPlayers(playersDTO);
    }
    public void getTopAssists(int leagueId) {

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://api-football-v1.p.rapidapi.com/v3/players/topassists?league="+leagueId+"&season=2022"))
                .header("x-rapidapi-host", "api-football-v1.p.rapidapi.com")
                .header("x-rapidapi-key", getToken())
                .build();
        HttpResponse<String> response = null;
        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        assert response != null;
        System.out.println(response.body());
        ObjectMapper objectMapper = new ObjectMapper();
        PlayersDTO playersDTO = new PlayersDTO();
        try {
            playersDTO = objectMapper.readValue(response.body(), PlayersDTO.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        insertPlayers(playersDTO);
    }

    public void insertPlayers(PlayersDTO playersDTO) {
        Optional<Tournament> optionalTournament = tournamentRepository.findById(playersDTO.getParameters().getLeague());
        List<Player> players = optionalTournament.get().getPlayers();
        playersDTO.getResponse().forEach(response -> {
            Player player=new Player();

            for (Player player1 : players) {
                if (player1.getPlayerId().equals(response.getPlayer().getId())){
                    player=player1;
                }
            }
            player.setTournament(List.of(optionalTournament.get()));
            player.setPlayerId(response.getPlayer().getId());
            player.setCountry(response.getPlayer().getBirth().getCountry());
            player.setAge(response.getPlayer().getAge());
            player.setName(response.getPlayer().getFirstname() + " " + response.getPlayer().getLastname());
            player.setLogo(response.getPlayer().getLogo());
            player.setGames(response.getStatistics().get(0).getGames().getAppearences());
            player.setAssists(response.getStatistics().get(0).getGoals().getAssists());
            player.setGoals(response.getStatistics().get(0).getGoals().getGoals());
            player.setPosition(response.getStatistics().get(0).getGames().getPosition());

//            for (Team team : teamList) {
//                if (response.getStatistics().get(0).getTeam().getId().equals(team.getTeamId())) {
//                    player.setTeam(team);
//                }
//            }
            Optional<Team> optionalTeam = teamRepository.findByTeamIdAndTournamentName(response.getStatistics().get(0).getTeam().getId(),response.getStatistics().get(0).getLeague().getName());
            if (optionalTeam.isPresent()) {
                Team team = optionalTeam.get();
                player.setTeam(team);
            }
            playerRepository.save(player);
        });

    }

    public StringBuilder getGoalScore(String tounamentName){
        StringBuilder players=new StringBuilder();

        int count=0;
        players.append("To'purarlar \n").append(tounamentName).append("\n");
        players.append("№  Name             G  As O'").append("\n").append("----------------------------\n");

        for (Player player : basedService.getAssistScore(tounamentName)) {

            if (player.getGoals() != 0) {
                count++;
                if (count < 10) {
                    players.append(count).append("  ");
                } else {
                    players.append(count).append(" ");
                }


                if (player.getName().length() > 15) {
                    players.append(player.getName(), 0, 15);

                } else {
                    int length = 15 - player.getName().length();
                    players.append(player.getName());
                    players.append(" ".repeat(Math.max(0, length)));
                }


                if (player.getGoals() < 10) {
                    players.append("  ").append(player.getGoals());
                } else {
                    players.append(" ").append(player.getGoals());
                }
                if (player.getAssists() < 10) {
                    players.append("  ").append(player.getAssists());
                } else {

                    players.append(" ").append(player.getAssists());
                }
                if (player.getGames() < 10) {
                    players.append("  ").append(player.getGames());
                } else {
                    players.append(" ").append(player.getGames());
                }

                    players.append("\n");

            }
        }
        return players;

    }
    public StringBuilder getAssistScore(String tounamentName){
        StringBuilder players=new StringBuilder();

        int count=0;
        players.append("Assistentlar \n").append(tounamentName).append("\n");
        players.append("№  Name             As G O'").append("\n").append("----------------------------\n");

        for (Player player : basedService.getGoalScore(tounamentName)) {

            if (player.getAssists() != 0) {
                count++;
                if (count < 10) {
                    players.append(count).append("  ");
                } else {
                    players.append(count).append(" ");
                }


                if (player.getName().length() > 15) {
                    players.append(player.getName(), 0, 15);

                } else {
                    int length = 15 - player.getName().length();
                    players.append(player.getName());
                    players.append(" ".repeat(Math.max(0, length)));
                }

                if (player.getAssists() < 10) {
                    players.append("  ").append(player.getAssists());
                } else {

                    players.append(" ").append(player.getAssists());
                }

                if (player.getGoals() < 10) {
                    players.append("  ").append(player.getGoals());
                } else {
                    players.append(" ").append(player.getGoals());
                }


                if (player.getGames() < 10) {
                    players.append("  ").append(player.getGames());
                } else {
                    players.append(" ").append(player.getGames());
                }

                    players.append("\n");

            }
        }
        return players;

    }


}
