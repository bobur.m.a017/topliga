package com.topliga.service;


import com.topliga.bot.botService.buttons.InlineKeyboardService;
import com.topliga.entity.tournament.Tournament;
import com.topliga.repository.TournamentRepository;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.util.List;

@Service
public class TournamentService {
    private final StandingsService standingsService;
    private final InlineKeyboardService inlineKeyboardService;
    private final MatchService matchService;
    private  final PlayerService playerService;
    private final TournamentRepository tournamentRepository;
    private final GamesServices gamesServices;

    public TournamentService(StandingsService standingsService, InlineKeyboardService inlineKeyboardService, MatchService matchService, PlayerService playerService, TournamentRepository tournamentRepository, GamesServices gamesServices) {
        this.standingsService = standingsService;
        this.inlineKeyboardService = inlineKeyboardService;
        this.matchService = matchService;
        this.playerService = playerService;
        this.tournamentRepository = tournamentRepository;
        this.gamesServices = gamesServices;
    }

    public EditMessageText tournamentType(Update update) {

        EditMessageText editmessageText = new EditMessageText();
        CallbackQuery callbackQuery = update.getCallbackQuery();
        editmessageText.setParseMode(ParseMode.HTML);
        editmessageText.setChatId(callbackQuery.getMessage().getChatId().toString());
        Integer messageId1 = callbackQuery.getMessage().getMessageId();
        editmessageText.setMessageId(messageId1);
        String data = callbackQuery.getData();
        String text = callbackQuery.getMessage().getText();
        String[] tournamentName = standingsService.getTournamentName(data);

        StringBuilder tableTournament=new StringBuilder();
        if (data.contains("_TurnirJadvali")) {
            tableTournament = standingsService.getTableTournament(tournamentName[0]);
        } else if (data.contains("_O'tganO'yinlar")) {
            tableTournament = new StringBuilder(matchService.getOldMatches(tournamentName[0]));
        } else if (data.contains("_KeyingiO'yinlar")) {
            tableTournament = new StringBuilder(matchService.getNewMatches(tournamentName[0]));
        } else if (data.contains("_To'purarlar")) {
            tableTournament=new StringBuilder(playerService.getGoalScore(tournamentName[0]));
        } else if (data.contains("_Assistentlar")) {
            tableTournament=new StringBuilder(playerService.getAssistScore(tournamentName[0]));
        } else if (data.contains("_prev")) {
            Tournament tournament = buttonControl(tournamentName, -1);
            tableTournament= standingsService.getTableTournament(tournament.getName());

        } else if (data.contains("_next")) {
            Tournament tournament = buttonControl(tournamentName, 1);
            tableTournament= standingsService.getTableTournament(tournament.getName());
        }
        InlineKeyboardMarkup inlineKeyboardMarkup=null;
        if (tournamentName.length>2) {
            inlineKeyboardMarkup = inlineKeyboardService.keyboardForStatisticTable(tournamentName[0], Integer.valueOf(tournamentName[2]));
        }
        if (data.contains("_Prev")) {
            coupleButton(tournamentName,-10);
            tableTournament = new StringBuilder(standingsService.getLast100Matches(tournamentName[0]));
            inlineKeyboardMarkup=inlineKeyboardService.getLast100Match(tournamentName[0]);
        } else if (data.contains("_Next")) {
            coupleButton(tournamentName,10);
            tableTournament = new StringBuilder(standingsService.getLast100Matches(tournamentName[0]));
            inlineKeyboardMarkup=inlineKeyboardService.getLast100Match(tournamentName[0]);
        }
        if (data.contains("_preV")) {
            coupleButton2(tournamentName,-10);
            tableTournament = new StringBuilder(gamesServices.getMatchesLive(tournamentName[0]));
            inlineKeyboardMarkup=inlineKeyboardService.getLast50Match(tournamentName[0]);
        } else if (data.contains("_nexT")) {
            coupleButton2(tournamentName,10);
            tableTournament = new StringBuilder(gamesServices.getMatchesLive(tournamentName[0]));
            inlineKeyboardMarkup=inlineKeyboardService.getLast50Match(tournamentName[0]);
        }
        editmessageText.setText("<pre>"+tableTournament.toString()+"</pre>");
        editmessageText.setReplyMarkup(inlineKeyboardMarkup);
        editmessageText.setMessageId(callbackQuery.getMessage().getMessageId());
        return editmessageText;
    }
    public SendMessage last10Match(Update update){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setParseMode(ParseMode.HTML);
        sendMessage.setChatId(update.getMessage().getChatId().toString());
        sendMessage.setReplyMarkup(inlineKeyboardService.getLast100Match("0"));
        sendMessage.setText("<pre>"+ standingsService.getLast100Matches("0") +"</pre>");
        return sendMessage;
    }

    public void coupleButton(String[] tournamentName,Integer index){

        String before=tournamentName[0];
        int bef = Integer.parseInt(before)+index;
        if (bef<0){
            bef=0;
        }
        else if (bef>90){
            bef=90;
        }
        tournamentName[0]=String.valueOf(bef);
    }
    public void coupleButton2(String[] tournamentName,Integer index){

        String before=tournamentName[0];
        int bef = Integer.parseInt(before)+index;
        if (bef<0){
            bef=0;
        }
        else if (bef>50){
            bef=50;
        }
        tournamentName[0]=String.valueOf(bef);
    }



    public Tournament buttonControl(String[] tournamentName,Integer plusOrMinus){
        List<Tournament> tournamentList = tournamentRepository.findAll();
        String before=tournamentName[2];
        int bef = Integer.parseInt(before);
        bef=bef+plusOrMinus;
        if (bef<0){
            bef=tournamentList.size()-1;
        }
        else if (bef>tournamentList.size()-1){
            bef=0;
        }
        tournamentName[2]=String.valueOf(bef);
        String newTournamentName = tournamentList.get(bef).getName();
        tournamentName[0]=newTournamentName;

        return tournamentList.get(bef);
    }
    public SendMessage liveScore(Update update){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setParseMode(ParseMode.HTML);
        sendMessage.setChatId(update.getMessage().getChatId().toString());
        sendMessage.setReplyMarkup(inlineKeyboardService.getLast50Match("0"));
        sendMessage.setText("<pre>"+ gamesServices.getMatchesLive("0") +"</pre>");
        return sendMessage;
    }


}

