package com.topliga.service;

import com.topliga.DTO.statsDTO.StatsDTO;
import com.topliga.entity.team.Team;
import com.topliga.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Component
public class StatsServices {
    private final BasedService basedService;
    private final TeamRepository teamRepository;

    @Autowired
    public StatsServices(BasedService basedService, TeamRepository teamRepository) {
        this.basedService = basedService;

        this.teamRepository = teamRepository;
    }


    public StatsDTO getStats(String tournamentName) {
        StatsDTO statsDTO = new StatsDTO();
        List<Team> allByTournamentName = teamRepository.findAllByTournamentNameOrderByGroupAscPointDesc(tournamentName);
        statsDTO.setTeams(allByTournamentName);
        statsDTO.setGoalScorers(basedService.getGoalScore(tournamentName));
        statsDTO.setAssistScorers(basedService.getAssistScore(tournamentName));
        statsDTO.setOldMatches(basedService.getOldMatch(tournamentName));
        statsDTO.setNextMatches(basedService.getNoStartedMatch(tournamentName));
        return statsDTO;
    }
}
