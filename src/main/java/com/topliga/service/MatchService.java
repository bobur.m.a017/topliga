package com.topliga.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.topliga.DTO.matchesDTO.MatchesDTO;
import com.topliga.entity.country.Country;
import com.topliga.entity.match.Away;
import com.topliga.entity.match.Home;
import com.topliga.entity.match.Match;
import com.topliga.entity.team.Team;
import com.topliga.entity.tournament.Tournament;
import com.topliga.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class MatchService implements Token{
    private final MatchRepository matchRepository;
    private final HomeRepository homeRepository;
    private final AwayRepository awayRepository;
    private final TeamRepository teamRepository;
    private final TournamentRepository tournamentRepository;
    private final BasedService basedService;
    @Autowired
    private CountryRepository countryRepository;


    @Autowired
    public MatchService(MatchRepository matchRepository, HomeRepository homeRepository, AwayRepository awayRepository, TeamRepository teamRepository, TournamentRepository tournamentRepository, BasedService basedService) {
        this.matchRepository = matchRepository;
        this.homeRepository = homeRepository;
        this.awayRepository = awayRepository;
        this.teamRepository = teamRepository;
        this.tournamentRepository = tournamentRepository;
        this.basedService = basedService;
    }

    public void getMatches(int leagueId){

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://api-football-v1.p.rapidapi.com/v3/fixtures?league="+leagueId+"&season=2022&timezone=Asia%2FTashkent"))
                .header("x-rapidapi-host", "api-football-v1.p.rapidapi.com")
                .header("x-rapidapi-key", getToken())
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = null;
        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        ObjectMapper objectMapper = new ObjectMapper();
        MatchesDTO match= new MatchesDTO();
        try {
            assert response != null;
            match = objectMapper.readValue(response.body(), MatchesDTO.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        matchInsert(match);
    }
    public void matchInsert(MatchesDTO matchesDTO){
        if (matchesDTO.getResponse().size() > 1) {
            matchesDTO.getResponse().forEach(response -> {
                Match match = new Match();
                Home home = new Home();
                Away away = new Away();

                Optional<Match> optionalMatch = matchRepository.findById(response.getFixture().getId());
                if (optionalMatch.isPresent()) {
                    match = optionalMatch.get();
                }
                match.setId(response.getFixture().getId());
                match.setDate(response.getFixture().getDate());
                match.setReferee(response.getFixture().getReferee());
                match.setTime(response.getFixture().getTimestamp());
                match.setStatus(response.getFixture().getStatus().getMatchStatus());
                match.setAreaName(response.getLeague().getCountry());
                match.setRound(response.getLeague().getRound());
                Optional<Tournament> optionalTournament = tournamentRepository.findById(response.getLeague().getId());
                Tournament tournament;
                if (optionalTournament.isPresent()){
                    tournament=optionalTournament.get();
                }else {
                    tournament = new Tournament();
                    tournament.setCountryName(response.getLeague().getCountry());
                    tournament.setLogo(response.getLeague().getLogo());
                    tournament.setName(response.getLeague().getName());
                    tournament.setId(response.getLeague().getId());
                    tournament.setSeason(response.getLeague().getSeason());
                    tournament = tournamentRepository.save(tournament);
                }
                Optional<Country> countryByName = countryRepository.getCountryByName(response.getLeague().getCountry());
                if (countryByName.isEmpty()){
                    Country country = new Country();
                    country.setName(response.getLeague().getCountry());
                    country.setLogo(response.getLeague().getLogo());
                    countryRepository.save(country);
                }
                match.setTournamentName(tournament.getName());
                match.setTournaments(tournament);

                home.setTeamName(response.getTeams().getHome().getName());
                home.setTeamId(response.getTeams().getHome().getId());
                home.setLogo(response.getTeams().getHome().getLogo());
                home.setGoals(response.getGoals().getHome());

                away.setTeamName(response.getTeams().getAway().getName());
                away.setTeamId(response.getTeams().getAway().getId());
                away.setLogo(response.getTeams().getAway().getLogo());
                away.setGoals(response.getGoals().getAway());

                homeRepository.save(home);
                awayRepository.save(away);
                match.setHome(home);
                match.setAway(away);
                matchRepository.save(match);
            });
        }
    }
    public void matchToTeam(){
        List<Match> matchList = matchRepository.findAll();
        List<Team> teamList = teamRepository.findAll();
        for (Team team : teamList) {
            List<Match> matches=new ArrayList<>();
            for (Match match : matchList) {
                if (team.getId().equals(match.getHome().getTeamId())|| team.getId().equals(match.getAway().getTeamId())) {
                    matches.add(match);
                }
            }
            team.setMatch(matches);
            teamRepository.save(team);
        }

    }
    public void matchToTournament(){
        List<Match> matchList = matchRepository.findAll();
        List<Tournament> tournamentList = tournamentRepository.findAll();
        matchList.forEach(match -> {
        });
    }
    public StringBuilder getOldMatches(String tournament){
        List<Match> matchList = basedService.getOldMatch(tournament);
        StringBuilder matchText= new StringBuilder();
        matchText.append(tournament).append("\n");
        for (Match match : matchList) {

            matchText.append(getDate(match.getTime())).append("\n");
            matchText.append(match.getRound()).append("\n");
            matchText.append(match.getHome().getTeamName()).append("  ").append(match.getHome().getGoals())
                    .append(":").append(match.getAway().getGoals()).append("  ").append(match.getAway().getTeamName()).append("\n\n");
        }
        return matchText;
    }
    public StringBuilder getNewMatches(String tournament){
        List<Match> matchList = basedService.getNoStartedMatch(tournament);
        StringBuilder matchText= new StringBuilder();
        matchText.append(tournament).append("\n");
        for (Match match : matchList) {
            matchText.append(getDate(match.getTime())).append("\n");
            matchText.append(match.getRound()).append("\n");
            matchText.append(match.getHome().getTeamName()).append("  ").append(match.getHome().getGoals())
                    .append(":").append(match.getAway().getGoals()).append("  ").append(match.getAway().getTeamName()).append("\n\n");
        }
        return matchText;
    }
  public  String  getDate(Timestamp date1){
      long time = date1.getTime();
      Date date=new Date(time * 1000L);
      SimpleDateFormat newDate=new SimpleDateFormat("yyyy-MM-dd HH:mm");
      newDate.setTimeZone(TimeZone.getTimeZone("Asia/Tashkent"));
      return newDate.format(date);
  }
}
