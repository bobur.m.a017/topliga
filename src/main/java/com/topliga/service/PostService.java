package com.topliga.service;

import com.topliga.entity.posts.Post;
import com.topliga.repository.PostsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PostService {
    @Autowired
    PostsRepository postsRepository;
    public List<Post> getAllPosts(Integer page){
        Pageable pageable= PageRequest.of(0,page);
        return postsRepository.findAllByOrderByDateDesc(pageable);
    }
    public Post getAPostById(Integer id){
        Optional<Post> optionalPost = postsRepository.findById(id);
        if (optionalPost.isEmpty()) {
            return null;
        }
        return optionalPost.get();
    }
}
