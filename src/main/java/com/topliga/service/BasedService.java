package com.topliga.service;

import com.topliga.bot.botService.buttons.InlineKeyboardService;
import com.topliga.entity.match.Match;
import com.topliga.entity.player.Player;
import com.topliga.entity.tournament.Tournament;
import com.topliga.repository.MatchRepository;
import com.topliga.repository.PlayerRepository;
import com.topliga.repository.TeamRepository;
import com.topliga.repository.TournamentRepository;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class BasedService {

    private final MatchRepository matchRepository;
    private final PlayerRepository playerRepository;
    private final TournamentRepository tournamentRepository;


    public BasedService(TeamRepository teamRepository, StandingsService standingsService, InlineKeyboardService inlineKeyboardService, MatchRepository matchRepository, PlayerRepository playerRepository, TournamentRepository tournamentRepository) {
        this.matchRepository = matchRepository;
        this.playerRepository = playerRepository;
        this.tournamentRepository = tournamentRepository;
    }

    public List<Match> getOldMatch(String tournamentName) {
        Optional<List<Match>> allByTournamentNameOrderByDate = matchRepository.findAllByTournamentNameOrderByDate1(tournamentName);
        if (allByTournamentNameOrderByDate.isEmpty()) {
            return null;
        }
        List<Match> matchList = allByTournamentNameOrderByDate.get();
//        List<Match> matches = matchList.stream().filter(match -> "Match Finished".equals(match.getStatus())
//        ).collect(Collectors.toList());

//        Collections.reverse(matchList);
        int count=10;
        if (tournamentName.equals("UEFA Champions League")||tournamentName.equals("UEFA Europa League")){
            count=16;
        }
        List<Match> matches1 = new ArrayList<>();
        for (int i = 0; i < matchList.size(); i++) {
                matches1.add(matchList.get(i));
                if (count==i){
                    return matches1;
                }
        }
        return matches1;
    }

    public List<Match> getNoStartedMatch(String tournamentName) {
        Optional<List<Match>> allByTournamentNameOrderByDate = matchRepository.findAllByTournamentNameOrderByDate(tournamentName);
        if (allByTournamentNameOrderByDate.isEmpty()) {
            return null;
        }
        List<Match> matchList = allByTournamentNameOrderByDate.get();
//        List<Match> matches = matchList.stream().filter(match -> !"Match Finished".equals(match.getStatus()) && (match.getDate().getTime() > new Date().getTime())
//        ).collect(Collectors.toList());

        int count=10;
        if (tournamentName.equals("UEFA Champions League")||tournamentName.equals("UEFA Europa League")){
            count=16;
        }
        List<Match> matches1 = new ArrayList<>();
        for (int i = 0; i < matchList.size(); i++) {
            matches1.add(matchList.get(i));
            if (count==i){
                return matches1;
            }
        }
        return matches1;
    }

    public List<Player> getGoalScore(String tourNament) {
        Optional<Tournament> tournament = tournamentRepository.findByName(tourNament);
        if (tournament.isEmpty()) {
            return null;
        }
        List<Player> playerList = playerRepository.findAllByTournamentOrderByAssistsAscGoals(tournament.get());
        Collections.reverse(playerList);
        return playerList;
    }

    public List<Player> getAssistScore(String tourNament) {
        Optional<Tournament> tournament = tournamentRepository.findByName(tourNament);
        if (tournament.isEmpty()) {
            return null;
        }
        List<Player> playerList = playerRepository.findAllByTournamentOrderByGoalsAscAssists(tournament.get());
        Collections.reverse(playerList);
        return playerList;
    }
}
