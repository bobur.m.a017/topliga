package com.topliga.entity.posts;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
@Entity
@Data
@RequiredArgsConstructor
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 1024)
    private String link;

    @Column(length = 1024)
    private String name;
    @Column(length = 1024)
    private String header;

    private Timestamp date=Timestamp.valueOf(LocalDateTime.now());
    private Integer view;

}
