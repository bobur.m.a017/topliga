package com.topliga.entity.tournament;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.topliga.entity.BaseDatabase;
import com.topliga.entity.country.Country;
import com.topliga.entity.match.Match;
import com.topliga.entity.player.Player;
import com.topliga.entity.team.Team;
import lombok.*;

import javax.persistence.*;
import java.util.List;
@Data
@Entity
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Tournament extends BaseDatabase {

    private String countryName;

    private Integer season;
    @OneToMany
    @JsonIgnore
    private List<Match> matches;
    @ManyToMany
    @JsonIgnore
    private List<Team> teams;
    @ManyToOne
    @JsonIgnore
    private Country country;
    @JsonIgnore

    @JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @ManyToMany(mappedBy = "tournament")
    private List<Player> players;
}
