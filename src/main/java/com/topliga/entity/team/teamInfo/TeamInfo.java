package com.topliga.entity.team.teamInfo;

import com.topliga.entity.team.Team;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class TeamInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer played;
    private Integer win;
    private Integer draw;
    private Integer lose;
    private Integer goalsFor;
    private Integer goalsAgainst;

}
