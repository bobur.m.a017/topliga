package com.topliga.entity.team;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.topliga.entity.match.Match;
import com.topliga.entity.player.Player;
import com.topliga.entity.team.teamInfo.TeamInfo;
import com.topliga.entity.tournament.Tournament;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
@Data
@Entity
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer teamId;
    private String  name;
    private String  logo;
    private String  country;
    private String  tournamentName;
    private Integer season;
    private Integer standing;
    private Integer point;
    private Integer goalsDiff;
    private String  form;
    @Column(name = "grup")
    private String group;
    private Date date;
    @ManyToMany
    @JsonIgnore
    private List<Match> match;
    @JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)

    @ManyToMany(mappedBy = "teams")
    @JsonIgnore
    private List<Tournament> tournaments;
    @OneToOne(fetch = FetchType.EAGER)
    private TeamInfo teamInfos;
    @JsonIgnore
    @OneToMany(mappedBy = "team")
    private List<Player> player;

}
