package com.topliga.entity.match;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.topliga.entity.team.Team;
import com.topliga.entity.tournament.Tournament;
import lombok.*;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Data
@Entity
@ToString
public class Match  {
    @Id
    private Integer id;
    private Date date;
    private Timestamp time;
    private String referee;
    private String round;
    private String tournamentName;
    private String areaName;
    private String status;
    @JsonIgnore
    @JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @ManyToMany(mappedBy = "match")
    private List<Team> teams;
    @OneToOne(fetch = FetchType.EAGER)
    private Home home;
    @OneToOne(fetch = FetchType.EAGER)
    private Away away;
    @JsonIgnore


    @ManyToOne
    private Tournament tournaments;
}
