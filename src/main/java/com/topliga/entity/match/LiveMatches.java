package com.topliga.entity.match;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LiveMatches {
    @Id
    private Integer id;
    private String goals;
    private String teamH;
    private String logoTeamH;
    private String teamA;
    private String logoTeamA;
    private String time;
    private String tournamentName;

}
