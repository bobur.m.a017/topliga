package com.topliga.entity.player;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.topliga.entity.team.Team;
import com.topliga.entity.tournament.Tournament;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;
@Data
@Entity
@RequiredArgsConstructor
public class Player  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String logo;
    private Integer playerId;
    private Integer goals;
    private Integer assists;
    private String country;
    private Integer age;
    private Integer games;
    private String position;
    @ManyToMany
    @JsonIgnore
    private List<Tournament> tournament;
    @ManyToOne
    @JsonIgnore
    private Team team;


    public void setAssists(Integer assists) {
        if (assists == null){
            assists=0;
        }
        this.assists = assists;
    }
}
