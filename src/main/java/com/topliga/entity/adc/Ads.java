package com.topliga.entity.adc;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Ads {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true)
    private Integer number;
    private String name;
    private String link;
    private String header;
}
