package com.topliga.MVC.ads;
import com.topliga.MVC.requestMessage.Message;
import com.topliga.entity.adc.Ads;
import com.topliga.repository.AdsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/api/admin/ads")
@RestController
public class AdsController {
    private final AdsRepository adminRepository;

    @Autowired
    public AdsController(AdsRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    @PostMapping("/addAds")
    public Message addAdmin(Ads admin){
        adminRepository.save(admin);
        return Message.SUCCESS;
    }

    @GetMapping("/getAds{id}")
    public Ads getAdmin(@PathVariable("id")Integer id){
        return adminRepository.findById(id).orElse(null);
    }

    @PostMapping("/editAds{id}")
    public Message editAdmin(@PathVariable("id")Integer id, Ads admin){
        Optional<Ads> admin1 = adminRepository.findById(id);
        if (admin1.isEmpty()){
            return Message.SUCCESS_ERROR;
        }
        Ads admin2 = admin1.get();
        admin2.setName(admin.getName());
        admin2.setNumber(admin.getNumber());
        admin2.setName(admin.getName());
        adminRepository.save(admin2);
        return Message.SUCCESS;
    }

    @GetMapping("/getAds")
    public List<Ads> getAdmins(){
        return adminRepository.findAll();
    }

    @GetMapping("/deleteAds{id}")
    public Message deleteAdmin(@PathVariable("id")Integer id){
        adminRepository.deleteById(id);
        return Message.SUCCESS;
    }

}
