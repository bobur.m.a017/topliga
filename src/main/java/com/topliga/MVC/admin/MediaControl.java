package com.topliga.MVC.admin;

import com.topliga.MVC.requestMessage.Message;
import com.topliga.entity.media.Media;
import com.topliga.repository.MediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("/api/admin/media")
public class MediaControl {

    private final MediaRepository mediaRepository;


    @Autowired
    public MediaControl(MediaRepository mediaRepository) {
        this.mediaRepository = mediaRepository;

    }


    @GetMapping("/getMedias/{page}")
    public Page<Media> getAllMedia(@PathVariable("page") Integer page){
        Page<Media> allBy;
        Pageable pageable = PageRequest.of(0, page);
        allBy = mediaRepository.findAllByOrderByDate(pageable);
        return allBy;
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    @PutMapping("/editMedia")
    public Message editMedia(@RequestBody Media media){
        mediaRepository.save(media);
        return Message.SUCCESS;
    }


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    @PostMapping("/addMedia")
    public Message addMedia(@RequestBody Media media) {
        mediaRepository.save(media);
        return Message.SUCCESS;
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    @GetMapping("/getMedia/{id}")
    public Media getMedia(@PathVariable("id") Integer id){
        Optional<Media> optionalMedia = mediaRepository.findById(id);
        return optionalMedia.orElse(null);
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    @GetMapping("/deleteMedia/{id}")
    public Message deleteMedia(@PathVariable("id") Integer id){
        mediaRepository.deleteById(id);
        return Message.SUCCESS;
    }

}
