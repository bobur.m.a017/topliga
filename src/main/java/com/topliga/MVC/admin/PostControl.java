package com.topliga.MVC.admin;

import com.topliga.MVC.requestMessage.Message;
import com.topliga.entity.posts.Post;
import com.topliga.repository.PostsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/post")
public class PostControl {

    private final PostsRepository postsRepository;

    @Autowired
    public PostControl(PostsRepository postsRepository) {
        this.postsRepository = postsRepository;
    }


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    @GetMapping("/getPost/{id}")
    public Post getPost(
            @PathVariable("id") Integer id
    ){
        return postsRepository.findById(id).orElse(null);
    }




    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    @GetMapping("/getPosts/{page}")
    public List<Post> getPosts(@PathVariable("page") Integer page){

        Pageable pageable = PageRequest.of(page, 20);
        return postsRepository.findAllByOrderByDateDesc(pageable);
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    @PostMapping("/addPost")
    public Message addPost(@RequestBody Post post){
        postsRepository.save(post);
        return Message.SUCCESS;
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    @GetMapping("/deletePost/{id}")
    public Message deletPost(
            @PathVariable("id") Integer id
    ){
        postsRepository.deleteById(id);
        return Message.SUCCESS;
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    @PutMapping("/editPost")
    public Message editPost(
//            @PathVariable("id")Integer id,
            @RequestBody Post post
    ){
//        Post post1 = postsRepository.findById(id).get();
//        post1.setDate(post.getDate());
//        post1.setLink(post.getLink());
//        post1.setName(post.getName());
//        post1.setHeader(post.getHeader());
        postsRepository.save(post);
        return Message.SUCCESS;
    }

}
