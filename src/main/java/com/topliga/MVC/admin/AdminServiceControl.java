package com.topliga.MVC.admin;

import com.topliga.DTO.admin.AdminDTO;
import com.topliga.MVC.requestMessage.Message;
import com.topliga.entity.admin.Admin;
import com.topliga.entity.admin.Role;
import com.topliga.repository.AdminRepository;
import com.topliga.repository.RoleRepository;
import com.topliga.security.ApplicationUsernamePasswordAuthenticationFilter;
import com.topliga.security.ByUserDetailes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/admin/service")
public class AdminServiceControl {
    private final AdminRepository adminRepository;
    private final RoleRepository roleRepository;
    private final ByUserDetailes byUserDetailes;
    final PasswordEncoder passwordEncoder;
    private final ApplicationUsernamePasswordAuthenticationFilter app;

    @Autowired
    public AdminServiceControl(AdminRepository adminRepository, RoleRepository roleRepository, ByUserDetailes byUserDetailes, PasswordEncoder passwordEncoder, ApplicationUsernamePasswordAuthenticationFilter app) {
        this.adminRepository = adminRepository;
        this.roleRepository = roleRepository;
        this.byUserDetailes = byUserDetailes;
        this.passwordEncoder = passwordEncoder;
        this.app = app;
    }


    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @ResponseBody
    @PostMapping("/addAdmin")
    public Message addAdmin(@RequestBody AdminDTO admindto) {
        Optional<Admin> byAdminName = adminRepository.findByAdminNameAndPhoneNumber(admindto.getAdminName(), admindto.getPhoneNumber());
        if (byAdminName.isPresent()) {
            return Message.DOUBLECITE_ERROR;
        }
        Admin admin = new Admin();
        admin.setName(admindto.getName());
        admin.setAdminName(admindto.getAdminName());
        admin.setPassword(passwordEncoder.encode(admindto.getPassword()));
        admin.setEmail(admindto.getEmail());
        admin.setPhoneNumber(admindto.getPhoneNumber());
        Optional<Role> roleRepositoryByName = roleRepository.findByName(admindto.getRole());
        Role role;
        if (roleRepositoryByName.isEmpty()) {
            role = new Role();
            role.setId(2);
            role.setName(admindto.getRole());
            Role saveRole = roleRepository.save(role);
            admin.setRoles(Collections.singletonList(saveRole));
        } else
            admin.setRoles(Collections.singletonList(roleRepositoryByName.get()));
        adminRepository.save(admin);
        return Message.SUCCESS;
    }

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @GetMapping("/getAdmin/{id}")
    public Admin getAdmin(@PathVariable("id") Integer id) {
        return adminRepository.findById(id).orElse(null);
    }

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @ResponseBody
    @PostMapping("/editAdmin/{id}")
    public Message editAdmin(@PathVariable("id") Integer id, AdminDTO admin) {
        Optional<Admin> admin1 = adminRepository.findById(id);
        if (admin1.isEmpty()) {
            return Message.SUCCESS_ERROR;
        }
        Admin admin2 = admin1.get();
        admin2.setAdminName(admin.getAdminName());
        admin2.setPhoneNumber(admin.getPhoneNumber());
        admin2.setName(admin.getName());
        admin2.setPassword(passwordEncoder.encode(admin.getPassword()));
        Optional<Role> optionalRole = roleRepository.findByName(admin.getRole());

        Role role;
        if (optionalRole.isEmpty()) {
            role = new Role();
            role.setName(admin.getRole());
            Role save = roleRepository.save(role);
            role = save;
        } else
            role = optionalRole.get();
        admin2.setRoles(Collections.singletonList(role));
        adminRepository.save(admin2);
        return Message.SUCCESS;
    }

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @GetMapping("/")
    public List<Admin> getAdmins() {
        return adminRepository.findAll();
    }

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @GetMapping("/deleteAdmin/{id}")
    public Message deleteAdmin(@PathVariable("id") Integer id) {
        adminRepository.deleteById(id);
        return Message.SUCCESS;
    }


    @PostMapping("/login")
    public String login(HttpServletRequest req, HttpServletResponse res) {
        try {
            return app.successfulAuthentication(req, res);
        } catch (Exception e) {
            return "Xatolik";
        }

    }

}
