package com.topliga.MVC.user;

import com.topliga.DTO.statsDTO.StatsDTO;
import com.topliga.entity.match.LiveMatches;
import com.topliga.repository.LiveMatchesRepository;
import com.topliga.service.StatsServices;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class StatsController {

   private final StatsServices statsServices;
   private final LiveMatchesRepository liveMatchesRepository;

    public StatsController(StatsServices statsServices, LiveMatchesRepository liveMatchesRepository) {
        this.statsServices = statsServices;
        this.liveMatchesRepository = liveMatchesRepository;
    }


    @GetMapping("/getStats/{tournamentName}")
    public StatsDTO getStats (@PathVariable("tournamentName") String tournamentName){
        return statsServices.getStats(tournamentName);
    }

    @GetMapping("/getLiveMatches")
    public List<LiveMatches> getLiveMatches (){
        return liveMatchesRepository.findAllByOrderByIdAsc();

    }
}
