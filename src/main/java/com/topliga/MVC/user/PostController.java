package com.topliga.MVC.user;

import com.topliga.entity.posts.Post;
import com.topliga.repository.PostsRepository;
import com.topliga.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/user/post")
public class PostController {
    private final PostService postService;
    private final PostsRepository postsRepository;
    @Autowired
    public PostController(PostService postService, PostsRepository postsRepository) {
        this.postService = postService;
        this.postsRepository = postsRepository;
    }



    @GetMapping("/getPosts/{pageNumber}")
    public List<Post> getAllPosts(@PathVariable("pageNumber") Integer page) {


        List<Post> allPosts = postService.getAllPosts(page);
        return allPosts;
    }

    @GetMapping("/getPost/{id}")
    public Post getAPost(@PathVariable("id") int postId){
        return postService.getAPostById(postId);
    }

}
