package com.topliga.MVC.requestMessage;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Message {
    SUCCESS("Muvaffaqiyatli amalga oshdi",200),
    SUCCESS_ERROR("jARAYON AMALGA OSHMADI",400),
    NOT_FOUND("Bunaqa buyruq topilmadi",403),
    OTHER_ERROR("Xatolik",500),
    DOUBLECITE_ERROR("Bunaqa mavjud",500);

    private String message;
    private Integer numberMessage;
}
