package com.topliga.repository;

import com.topliga.entity.match.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface MatchRepository extends JpaRepository<Match,Integer> {

    @Query(value = "select * from match where match.date > NOW() and match.tournament_name = ?1  order by match.date asc limit 100",nativeQuery = true)
    Optional<List<Match>> findAllByTournamentNameOrderByDate(String tournamentName);

    @Query(value = "select * from match where match.date < NOW() and match.tournament_name = ?1  order by match.date desc limit 100",nativeQuery = true)
    Optional<List<Match>> findAllByTournamentNameOrderByDate1(String tournamentName);

    @Query(
            value = "SELECT * FROM MATCH m WHERE m.status = 'Match Finished' and m.date < NOW() ORDER BY DATE DESC LIMIT 100",
            nativeQuery = true)
    List<Match> findLast100Match();
}