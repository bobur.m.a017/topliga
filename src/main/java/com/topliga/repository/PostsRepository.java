package com.topliga.repository;

import com.topliga.entity.posts.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PostsRepository extends JpaRepository<Post,Integer> {

//    @Query(value = "select * from Post ORDER BY DATE",nativeQuery = true)
    List<Post> findAllByOrderByDateDesc(Pageable pageable);

}
