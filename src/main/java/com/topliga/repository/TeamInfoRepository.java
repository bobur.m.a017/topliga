package com.topliga.repository;

import com.topliga.entity.team.Team;
import com.topliga.entity.team.teamInfo.TeamInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamInfoRepository extends JpaRepository<TeamInfo,Integer> {

}
