package com.topliga.repository;

import com.topliga.entity.media.Media;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MediaRepository extends JpaRepository<Media,Integer> {



    Page<Media> findAllByOrderByDate(Pageable pageable);
}
