package com.topliga.repository;

import com.topliga.entity.adc.*;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdsRepository extends JpaRepository<Ads,Integer> {

}
