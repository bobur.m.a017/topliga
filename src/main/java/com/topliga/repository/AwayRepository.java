package com.topliga.repository;

import com.topliga.entity.match.Away;
import com.topliga.entity.match.Home;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AwayRepository extends JpaRepository<Away,Integer> {
}
