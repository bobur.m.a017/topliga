package com.topliga.repository;

import com.topliga.entity.match.Home;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HomeRepository extends JpaRepository<Home,Integer> {
}
