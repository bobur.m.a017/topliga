package com.topliga.repository;

import com.topliga.entity.team.Team;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TeamRepository extends JpaRepository<Team,Integer> {
    List<Team> findAllByTournamentName(String tournamentName);
Optional<Team> findByTeamIdAndTournamentName(Integer teamId, String tournamentName);
List<Team> findAllByTournamentNameOrderByGroupAscPointDesc(String tournamentName);

}
