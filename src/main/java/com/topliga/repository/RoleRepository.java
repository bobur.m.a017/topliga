package com.topliga.repository;

import com.topliga.entity.admin.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role,Integer> {
    Optional<Role>findByName(String roleName);
}
