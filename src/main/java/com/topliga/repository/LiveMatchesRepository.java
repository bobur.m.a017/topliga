package com.topliga.repository;

import com.topliga.entity.match.LiveMatches;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LiveMatchesRepository extends JpaRepository<LiveMatches,Integer> {

    List<LiveMatches> findAllByOrderByIdAsc();
    Page<LiveMatches> findAllBy(Pageable pageable);
}
