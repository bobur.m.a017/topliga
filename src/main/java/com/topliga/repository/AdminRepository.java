package com.topliga.repository;

import com.topliga.entity.admin.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminRepository extends JpaRepository<Admin,Integer> {

    Optional<Admin> findByAdminNameAndPhoneNumber(String name,String phone);
    Optional<Admin> findByPhoneNumber(String number);
}
