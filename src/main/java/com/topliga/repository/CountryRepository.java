package com.topliga.repository;

import com.topliga.entity.country.Country;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CountryRepository extends JpaRepository<Country, Integer> {
    Optional<Country> getCountryByName(String name);
}
