package com.topliga.repository;

import com.topliga.entity.match.Away;
import com.topliga.entity.player.Player;
import com.topliga.entity.tournament.Tournament;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlayerRepository extends JpaRepository<Player,Integer> {
    List<Player> findAllByTournamentOrderByAssistsAscGoals(Tournament tournament);


    List<Player> findAllByTournamentOrderByGoalsAscAssists(Tournament tournament);
}
