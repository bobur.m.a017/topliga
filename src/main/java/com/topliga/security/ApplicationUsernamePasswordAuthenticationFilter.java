package com.topliga.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.topliga.DTO.admin.AdminLogin;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
@Service
public class ApplicationUsernamePasswordAuthenticationFilter {

    private final PasswordEncoder passwordEncoder;
    private final ByUserDetailes byUserDetailes;
    private final AuthenticationManager authenticationManager;

    private String secretKey = "HJAtS9ALVpOPTCXw5W0Ifx2sHcBwmTKNKaqTgwD4";

    //
    // @Value("${jwt.expiration.date}")
    private String expirationDate = "86_400_000L";


    public ApplicationUsernamePasswordAuthenticationFilter(PasswordEncoder passwordEncoder,
                                                           ByUserDetailes byUserDetailes, AuthenticationManager authenticationManager) {
        this.passwordEncoder = passwordEncoder;
        this.byUserDetailes = byUserDetailes;
        this.authenticationManager = authenticationManager;
    }

    public Authentication attemptAuthentication(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws AuthenticationException {
        ObjectMapper objectMapper = new ObjectMapper();
        Authentication authentication = null;
        try {
            AdminLogin userSignInReceiveModel
                    = objectMapper.readValue(request.getInputStream(), AdminLogin.class);

            authentication = new UsernamePasswordAuthenticationToken(
                    userSignInReceiveModel.getPhoneNumber(),
                    userSignInReceiveModel.getPassword()
            );
            return authenticationManager.authenticate(authentication);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return authentication;
    }

    public String successfulAuthentication(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException, ServletException {

        ObjectMapper objectMapper = new ObjectMapper();
        System.out.println(secretKey);
        AdminLogin adminLogin
                = objectMapper.readValue(request.getInputStream(), AdminLogin.class);
        UserDetails userDetails = byUserDetailes.loadUserByUsername(adminLogin.getPhoneNumber());
                    if (!passwordEncoder.matches(adminLogin.getPassword(),userDetails.getPassword())){
                return "Xatolik parol yoki Telefon raqam xato";
            }
        String token = Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + 86_400_000L)) // 1 kun
                .setSubject(userDetails.getUsername())
                .compact();

        token="Bearer " + token;
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(userDetails,
                            null,
                            userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        return token;
    }

}
