package com.topliga.security;

import com.topliga.entity.admin.Admin;
import com.topliga.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ByUserDetailes implements UserDetailsService {


    final AdminRepository adminRepository;

    @Autowired
    public ByUserDetailes(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<Admin> byPhoneNumber = adminRepository.findByPhoneNumber(s);
        return byPhoneNumber.get();
    }
}
