package com.topliga.DTO.admin;

import lombok.Data;

@Data
public class AdminDTO {
    private String name;
    private String adminName;
    private String phoneNumber;
    private String password;
    private String role;
    private String email;
}
