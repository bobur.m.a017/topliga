package com.topliga.DTO.admin;

import lombok.Data;

@Data
public class AdminLogin {
    private String adminName;
    private String phoneNumber;
    private String password;

}
