package com.topliga.DTO.statsDTO;

import com.topliga.entity.match.Match;
import com.topliga.entity.player.Player;
import com.topliga.entity.team.Team;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatsDTO {
    private List<Player> goalScorers;
    private List<Player> assistScorers;
    private List<Team> teams;
    private List<Match> oldMatches;
    private List<Match> nextMatches;
}
