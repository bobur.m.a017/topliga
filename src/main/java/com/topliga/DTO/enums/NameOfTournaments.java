package com.topliga.DTO.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum NameOfTournaments {

    FRANCE(61,"League 1"),
//    FRANCE_CUP(66,"FRANCE_CUP"),
    England(39,"Premier League"),
//    England_CUP(45,"FAC"),
    Italy(135,"Serie A"),
//    Italy_Cup(137,"CUP"),
    Germany(78,"Bundesliga 1"),
//    Germany_Cup(81,"GermanyCup"),
    Spain(140,"LaLiga"),
//    Spain_Cup(143,"SpainCup"),
    CHAMPIONS_LEAGUE(2,"CHAMPIONS LEAGUE"),
//    EUROPE_LEAGUE(3,"EUROPE LEAGUE"),
    UZBEKISTAN(369,"SUPER LEAGUE");
//    UZBEKISTAN_CUP(802,"SUPER CUP");

    private Integer leagueId;
    private String leagueName;
}
