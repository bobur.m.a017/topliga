package com.topliga.DTO.matchesDTO.response.fixture.status;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Status {
    @JsonProperty("long")
    private String matchStatus;
    @JsonProperty("short")
    private String shorts;
    private int elapsed;
}
