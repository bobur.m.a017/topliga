package com.topliga.DTO.matchesDTO.response.teams.home;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Home {
    private int id;
    private String name;
    private String logo;
    private boolean winner;
}
