package com.topliga.DTO.matchesDTO.response.fixture;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.topliga.DTO.matchesDTO.response.fixture.status.Status;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Fixture {
    private int id;
    private String referee;
    private String timezone;
    @JsonFormat(timezone = "GMT+05:00")
    private Date date;
    private Timestamp timestamp;
    private Status status;
}
