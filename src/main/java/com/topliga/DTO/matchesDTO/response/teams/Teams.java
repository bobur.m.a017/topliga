package com.topliga.DTO.matchesDTO.response.teams;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.topliga.DTO.matchesDTO.response.teams.away.Away;
import com.topliga.DTO.matchesDTO.response.teams.home.Home;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Teams {
    private Home home;
    private Away away;
}
