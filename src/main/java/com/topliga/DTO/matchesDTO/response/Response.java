package com.topliga.DTO.matchesDTO.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.topliga.DTO.matchesDTO.response.fixture.Fixture;
import com.topliga.DTO.matchesDTO.response.goals.Goals;
import com.topliga.DTO.matchesDTO.response.leauge.League;
import com.topliga.DTO.matchesDTO.response.teams.Teams;
import lombok.Data;

import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Response {
    private Fixture fixture;
    private Goals goals;
    private League league;
    private Teams teams;

}
