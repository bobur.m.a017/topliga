package com.topliga.DTO.matchesDTO.response.leauge;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class League {
    private int id;
    private String name;
    private String country;
    private String logo;
    private String flag;
    private int season;
    private String round;
}
