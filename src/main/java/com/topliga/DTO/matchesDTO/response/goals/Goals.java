package com.topliga.DTO.matchesDTO.response.goals;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Goals {
    private int home;
    private int away;
}
