package com.topliga.DTO.matchesDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.topliga.DTO.matchesDTO.response.Response;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MatchesDTO {
   private List<Response> response;
}
