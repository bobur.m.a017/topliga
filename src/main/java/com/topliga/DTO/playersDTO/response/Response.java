package com.topliga.DTO.playersDTO.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.topliga.DTO.playersDTO.response.player.Player;
import com.topliga.DTO.playersDTO.response.stats.Statistics;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class Response {
    private Player player;
    private List<Statistics> statistics;
}
