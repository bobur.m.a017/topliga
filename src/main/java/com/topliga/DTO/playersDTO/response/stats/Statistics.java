package com.topliga.DTO.playersDTO.response.stats;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.topliga.DTO.playersDTO.response.stats.games.Games;
import com.topliga.DTO.playersDTO.response.stats.goals.Goals;
import com.topliga.DTO.playersDTO.response.stats.league.League;
import com.topliga.DTO.playersDTO.response.stats.team.Team;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class Statistics {
    private Games games;
    private Goals goals;
    private League league;
    private Team team;

}
