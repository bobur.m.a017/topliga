package com.topliga.DTO.playersDTO.response.player;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.topliga.DTO.playersDTO.response.player.birth.Birth;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class Player {
    private Integer id;
    private Integer age;
    private String firstname;
    private String lastname;
    @JsonProperty("photo")
    private String logo;
    private Birth birth;
}
