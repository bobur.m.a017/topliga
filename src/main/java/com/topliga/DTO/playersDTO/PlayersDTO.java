package com.topliga.DTO.playersDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.topliga.DTO.playersDTO.response.Response;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class PlayersDTO {
    private Parameters parameters;
    private List<Response> response;
}
