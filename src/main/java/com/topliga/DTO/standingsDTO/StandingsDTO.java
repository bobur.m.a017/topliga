package com.topliga.DTO.standingsDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.topliga.DTO.standingsDTO.main.Response;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class StandingsDTO {
    private Parameters parameters;
    private List<Response> response;
}
