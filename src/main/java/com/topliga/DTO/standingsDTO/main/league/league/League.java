package com.topliga.DTO.standingsDTO.main.league.league;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.topliga.DTO.standingsDTO.main.league.league.standings.Standings;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class League {
    private Integer id;
    private String name;
    private String country;
    private String logo;
    private String flag;
    private Integer season;
    private List<List<Standings>> standings;
}
