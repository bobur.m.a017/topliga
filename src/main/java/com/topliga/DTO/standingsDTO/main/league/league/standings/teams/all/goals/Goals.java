package com.topliga.DTO.standingsDTO.main.league.league.standings.teams.all.goals;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class Goals {
    @JsonProperty("for")
    private Integer goalsFor;
    @JsonProperty("against")
    private Integer goalsAgainst;
}
