package com.topliga.DTO.standingsDTO.main;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.topliga.DTO.standingsDTO.main.league.league.League;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {
private League league;
}
