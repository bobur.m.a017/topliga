package com.topliga.DTO.standingsDTO.main.league.league.standings.teams.all;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.topliga.DTO.standingsDTO.main.league.league.standings.teams.all.goals.Goals;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class All {
    private Integer played;
    private Integer win;
    private Integer draw;
    private Integer lose;
    private Goals goals;
}
