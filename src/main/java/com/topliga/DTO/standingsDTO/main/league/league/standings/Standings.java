package com.topliga.DTO.standingsDTO.main.league.league.standings;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.topliga.DTO.standingsDTO.main.league.league.standings.teams.Teams;
import com.topliga.DTO.standingsDTO.main.league.league.standings.teams.all.All;
import com.topliga.DTO.standingsDTO.main.league.league.standings.teams.team.Team;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class Standings {
    private Integer rank;
    private Integer points;
    private Integer goalsDiff;
    private String group;
    private String form;
    private String status;
    private String description;
    private Date update;
    private Team team;
    private All all;
}
