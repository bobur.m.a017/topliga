FROM openjdk:11-jre-slim-buster
EXPOSE 8899
ADD target/topliga.jar topliga.jar
ENTRYPOINT ["java","-jar","topliga.jar"]